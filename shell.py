#!/usr/bin/env python3
# coding: utf-8
#
# GPLv2 License
#
# Copyright (c) 2015 Nick
# Copyright (c) 2023 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#

__author__ = "nick"
import argparse
import os
import sys
import platform
import subprocess
import socket
import urllib.parse
from http.server import BaseHTTPRequestHandler, HTTPServer


class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        # avoid exception in BaseHTTPServer.py log_message() (thanks to user9837759 / https://stackoverflow.com/a/50499433) :
        self.client_address = ("",)
        parsed = urllib.parse.urlparse(self.path)
        print(self.path)
        if parsed.path.endswith("/index.html"):
            self.send_response(200)
            self.send_header("Content-Type", "text/html")
            self.end_headers()
            self.wfile.write(
                bytes(
                    """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Web Shell</title>
    <script >
function getOS(){
    fetch("OS")
    .then((response) => response.text())
    .then((txt) => { document.getElementById("OSINFO").innerText = txt });
}

function getUser(){
    fetch("user")
    .then((response) => response.text())
    .then((txt) => { document.getElementById("USERINFO").innerText = txt });
}

function init() {
    getOS();
    getUser();
    document.getElementById("commandForm").addEventListener("submit", runCommand);
}

function runCommand(event) {
    event.preventDefault();
    command = document.getElementById("command").value;
    fetch("run?command=" + encodeURIComponent(command))
    .then((response) => response.text())
    .then((txt) => { commandAndResult(command, txt); })
    .catch((error) => { commandAndResult(command, error.message, 'error'); });
}

function commandAndResult(command, result, resultClass='result') {
    const commandP = document.createElement('p');
    commandP.setAttribute('class', 'command');
    commandP.appendChild(document.createTextNode(command));
    const resultP = document.createElement('p');
    resultP.appendChild(document.createTextNode(result));
    resultP.setAttribute('class', resultClass);

    const commandHistory = document.getElementById("commandHistory");
    commandHistory.insertBefore(resultP, commandHistory.firstChild);
    commandHistory.insertBefore(commandP, commandHistory.firstChild);
}

    </script>
    <style type=text/css>
      .result, .command, .error {
        display: block;
        unicode-bidi: embed;
        font-family: monospace;
        white-space: pre;
      }
      .command {
        padding-left: 0.8em;
      }
      .command::before {
        content: "$ ";
        font-weight: bold;
      }
      .error {
        color: #f00;
      }
    </style>
</head>
<body onload="init()">
<div>
    &nbsp;OS Info: <div id="OSINFO" style="display: inline"></div>
    <br>
    &nbsp;Current User: <div id="USERINFO" style="display: inline"></div>
</div>
<div style="border: blueviolet 1px solid; margin: 1em; padding: 2em;">
    <form id="commandForm" >
        <label>
            Command
        <input type="text" name="command" id="command">
        <input type="submit" name="submit" class="button" id="submit_btn" value="send">
        </label>
    </form>
</div>
<br><br>

<div id="commandHistory" style="border: 1px solid #ccc; overflow: auto;"></div>
</body>
</html>
                    """,
                    "utf-8",
                )
            )

        elif parsed.path.endswith("/user"):
            userhome = os.path.expanduser("~")
            user = os.path.split(userhome)[-1]
            self.send_response(200)
            print("user is " + str(user))
            self.end_headers()
            self.wfile.write(bytes(user, "utf-8"))

        elif parsed.path.endswith("/OS"):
            returnMessage = str(sys.platform) + " / " + str(platform.platform())
            print(returnMessage)
            self.send_response(200)
            self.end_headers()
            self.wfile.write(bytes(returnMessage, "utf-8"))
            return

        elif parsed.path.endswith("/run"):
            print("in run")
            parsed = urllib.parse.urlparse(self.path)
            command = urllib.parse.parse_qs(parsed.query)["command"][0]
            print("command is " + command)
            try:
                commandOutput = subprocess.check_output(["(" + command + ") 2>&1"], shell=True)
                self.send_response(200)
                self.end_headers()
                self.wfile.write(commandOutput)
            except subprocess.CalledProcessError as err:
                print(err)
                self.send_response(200)
                self.end_headers()
                self.wfile.write(bytes("Error code : " + str(err.returncode) + "):\n", "utf-8") + err.output)

        else:
            self.send_response(404)
            self.end_headers()
        return


def parse_cli_args(args):
    parser = argparse.ArgumentParser(description="Simple HTTP web shell, running commands from your browser")
    parser.add_argument("--address", default="", help="IP address to bind to (by default: all addresses)")
    parser.add_argument("--port", default="4200", help="Port to listen to")
    parser.add_argument("--socket", default="", help="Socket to bind to")
    parser.add_argument("--daemonize", action="store_true", help="Fork and exit. Logs and errors are printed on calling console.")

    return parser.parse_args(args)


def start_server(args):
    if args.socket:
        try:
            os.remove(args.socket)
        except OSError:
            pass

        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.bind(args.socket)
        sock.listen(0)
        server = HTTPServer(args.socket, MyHandler, False)
        print("listening on " + args.socket)
        server.socket = sock

    elif args.port:
        server = HTTPServer((args.address, int(args.port)), MyHandler)
        print("listening on " + args.address + ":" + args.port)

    else:
        print("No port or socket given. Won't start server.")
        sys.exit(1)

    print("server probably started. Control page available under /index.html .")
    server.serve_forever()

    if args.socket:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        os.remove(args.socket)

    sys.exit(0)


def fork_once():
    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError:
        print("Forking failed")
        sys.exit(1)


def main():
    args = parse_cli_args(sys.argv[1:])

    if args.daemonize:
        fork_once()
        os.chdir("/")
        os.setsid()
        fork_once()

    start_server(args)


if __name__ == "__main__":
    main()
